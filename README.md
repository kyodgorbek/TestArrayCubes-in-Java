# TestArrayCubes-in-Java



import javax.swing.*;

public class TestArrayCubes{
  
  public static void main(String[] args){
   // Get array size
   String numStr = JOptionPane.showInputDialog("How many cubes?");
   int numCubes = Integer.parseInt(numCubes);
   
   // Create an ArrayCubes object with an array of this size
   ArrayCubes aC = new ArrayCubes(numCubes);
   
   //Fill the array
   aC.fillCubes();
   
   // Display the array contents
   JOptionPane.showInputDialog(null, "The first " + numCubes + "cubes are: \n" +aC.toString());
  }
 }  
   
